class MoviesController < ApplicationController
	
  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  	
  end

  def index
  
    @all_ratings = Movie.all_ratings
    @checked = Hash.new
    if params[:sort_by] == nil
      @movies = Movie.where("rating IN (:ratings)" , {:ratings => params.has_key?(:ratings) ? params[:ratings].keys : @all_ratings})    
			
    else
      @movies = Movie.where("rating IN (:ratings)" , :ratings => params.has_key?(:ratings) ? params[:ratings].keys : @all_ratings).find(:all, :order => params[:sort_by])
    end
#=begin
		    
    @all_ratings.each do |rating|
				if params.has_key?(:ratings)
					if params[:ratings].keys.include?(rating)
						@checked[rating] = true
					else
						@checked[rating] = false
					end
				else
    		  @checked[rating] = true
    	  end
    end  
#=end    
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
